import {Component, OnInit} from '@angular/core';
import {Service} from '../shared/service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(private service: Service, private router: Router, private route: ActivatedRoute) {

  }

  userName: any;
  password: any;

  login() {
    const body = {username: this.userName, password: this.password};


    this.service.post('user-service/login', JSON.stringify(body)).subscribe(response => {
      if (!response.data) {
        Swal.fire('', 'Login Error!', 'error');
      } else {
        localStorage.setItem('app-token', response.data.token);
        this.router.navigate(['/dashboard', {state: 'WAITING_CONFIRM'}]);
      }

    }, err => {
      Swal.fire('', 'Login Error!', 'error');
    });
  }

  ngOnInit() {
  }

}
